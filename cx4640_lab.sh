#/usr/bin/env bash

docker pull tobyisaac/cx4640:latest && docker run --rm -p 8888:8888 -v $PWD:/home/dev/cx4640 -it tobyisaac/cx4640:latest jupyter lab --no-browser --ip 0.0.0.0 --notebook-dir=/home/dev/cx4640
