---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Julia 1.6.1
  language: julia
  name: julia-1.6
---

# Syllabus

+++

| Who                          | **Instructor**                  | **Teaching Assistant** |
| ---                          | :------------------             | :------------------    |
| *name*                       | Toby Isaac                      | _TBD_                  |
| *email*                      | [tisaac@cc.gatech.edu][myemail] |                        |

+++

| What                   | Where                          | When                                   |
| --                     | :--                            | :---                                   |
| lectures               | [Instr. Center 215][InstrCntr] | Tuesdays and Thursdays, 2:00-2:15 p.m. |
| in-person office hours | [CODA S1323][codaloc]          | _TBD_                                  |
| virtual office hours   | [BlueJeans][bluejeans]         | _TBD_                                  |

+++

## About

Repository for notes for CX 4640 (Numerical Analysis I) for Fall 2021.

## Running

[InstrCntr]: https://map.gatech.edu/?id=82#!m/11007
[bluejeans]: https://gatech.bluejeans.com/5951177398
[codaloc]: https://codatechsquare.com/location/
[myemail]: mailto:tisaac@cc.gatech.edu
