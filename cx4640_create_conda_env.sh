#!/usr/bin/env bash

conda create -y -c conda-forge -n cx4640 --file requirements.txt \
  && julia -e "using Pkg; Pkg.add(\"IJulia\"); Pkg.precompile();"

