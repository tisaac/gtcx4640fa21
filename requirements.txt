jupyterlab==3.0.16
jupyter-server-proxy==3.0.2
nbconvert==5.6.1
jupyter-book==0.10.2
rise==5.7.1
jupyter_contrib_nbextensions==0.5.1
notebook==6.4.0
jupyter_nbextensions_configurator==0.4.1
hide_code==0.6.0

