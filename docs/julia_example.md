---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Julia 1.6.1
  language: julia
  name: julia-1.6
---

# Markdown Cell

```{code-cell}
using LinearAlgebra

A = rand(4,4)

LinearAlgebra.norm(A)
```
